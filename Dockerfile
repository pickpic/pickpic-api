FROM node:latest
RUN apt update && apt install imagemagick
COPY . /app
WORKDIR /app
RUN npm install --production
CMD node index.js
EXPOSE 3001
