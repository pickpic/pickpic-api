const express = require('express')
const http = require('http')
const compression = require('compression')
const faye = require('faye')
const path = require('path')
const _ = require('lodash')
const chokidar = require('chokidar')
const fs = require('fs-extra')
const async = require('async')
const qt = require('quickthumb')
const q = require('q')
const imgdir = path.resolve('./images')
const thumbdir = path.resolve('./thumbnails')
const classes = ['unclassified', 'good', 'bad']
let log = []
const files = {}
let loaded = false
classes.map(c => {
  if (!fs.existsSync(`${imgdir}/${c}`)) fs.mkdirpSync(`${imgdir}/${c}`)
})
if (!fs.existsSync(thumbdir)) fs.mkdirpSync(thumbdir)
const app = express()
const server = http.createServer(app)
const bayeux = new faye.NodeAdapter({ mount: '/api/faye' })
bayeux.attach(server)
const watcher = chokidar.watch(`${imgdir}/**`, { ignored: '.DS_Store', usePolling: true, useFsEvents: true, awaitWriteFinish: { stabilityThreshold: 400 }, interval: 30 })
watcher
  .on('add', (filepath, details) => {
    const p = filepath.replace(`${imgdir}/`, '')
    let [classification, file] = p.split('/')
    const from = files[file]
    files[file] = classification
    if (loaded) {
      log = log.filter(l => l.file !== file)
      log.push({ file, classification, from })
      log = _.takeRight(log, 30)
      bayeux.getClient().publish(`/changes`, { file, classification, from })
      console.log(`${file} marked as ${classification}`)
    }
  })
  .on('ready', async () => {
    loaded = true
    bayeux.getClient().publish(`/initial`, files)
    console.log(`Initial scan complete. Watching for changes.\n${_.keys(files).length} files scanned`)
  })
bayeux.on('handshake', id => {
  console.log(`Client with id ${id} has connected`)
})
app.get('/api/list', (req, res) => {
  res.send(files)
})
app.get('/api/log', (req, res) => {
  res.send(log)
})
app.get('/api/set/:id/:classification', async (req, res) => {
  const classification = files[req.params.id]
  if (!classification) {
    res.statusCode = 404
    res.send('File not found')
  } else {
    let src = `${imgdir}/${classification}/${req.params.id}`
    if (!await fs.exists(src)) {
      const others = classes.filter(c => c !== classification)
      for (let o of others) {
        const fn = `${imgdir}/${o}/${req.params.id}`
        const exists = await fs.exists(fn)
        if (exists) src = fn
      }
    }
    const lastlog = _.first(log.filter(l => l.file === req.params.id))
    await fs.move(src, `${imgdir}/${req.params.classification}/${req.params.id}`)
    res.send({ file: req.params.id, classification: files[req.params.id] })
  }
})
app.get('/api/thumbs/:id', async (req, res) => {
  const classification = files[req.params.id]
  if (!classification) {
    res.statusCode = 404
    res.send('File not found')
  } else {
    let src = `${imgdir}/${classification}/${req.params.id}`
    if (!await fs.exists(src)) {
      const others = classes.filter(c => c !== classification)
      for (let o of others) {
        const fn = `${imgdir}/${o}/${req.params.id}`
        const exists = await fs.exists(fn)
        if (exists) src = fn
      }
    }
    const dst = `${thumbdir}/${req.params.id}`
    fs.exists(dst).then(exists => {
      if (exists) res.sendFile(dst)
      else {
        const oldcons = console.log
        console.log = () => {}
        qt.convert({ src, dst, width: 200 }, (err, resp) => {
          if (err) {
            res.statusCode = 500
            res.send(err)
          } else {
            console.log = oldcons
            res.sendFile(resp)
          }
        })
      }
    })
  }
})
app.get('/api/images/:id', (req, res) => {
  const classification = files[req.params.id]
  if (!classification) {
    res.statusCode = 404
    res.send('File not found')
  } else {
    res.sendFile(`${imgdir}/${classification}/${req.params.id}`)
  }
})

server.listen(3001, () => {
  console.log('Started PickPic API Server')
})
